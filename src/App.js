import "./App.css";

import React, { useState } from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

import PhoneBtn from "./components/MainPage/PhoneBtn";

import Home from "./pages";
import Model2 from "./pages/model2";
import Model3 from "./pages/model3";
import Model4 from "./pages/model4";
import Model6 from "./pages/model6";
import Diane from "./pages/diane";

import Vacancy from "./pages/vacancy";

import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";
import Footer from "./components/Footer";
import Cookies from "universal-cookie";
import BtnToTop from "./components/MainPage/BtnToTop";

import News1 from "./pages/news1";
import News2 from "./pages/news2";
import News3 from "./pages/news3";

import Biogas from "./pages/biogas";
import Co2 from "./pages/co2";
import Cogeneration from "./pages/cogeneration";
import Gas from "./pages/gas";
import Powerengine from "./pages/powerengine";
import Trigeneration from "./pages/trigeneration";

import Project1 from "./pages/Project";
import Project2 from "./pages/Project2";
import Project3 from "./pages/Project3";
import Project4 from "./pages/Project4";

import { ReactQueryDevtools } from "@tanstack/react-query-devtools";
import NewsPage from "./pages/news";
import Project from "./pages/Project";
const cookies = new Cookies();
cookies.get("language")
  ? console.log("Selected language is ", cookies.get("language"))
  : cookies.set("language", "ru", { path: "/" });

function App() {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(!isOpen);
  };

  return (
    <>
      <Router>
        <Sidebar isOpen={isOpen} toggle={toggle} />
        <Navbar toggle={toggle} />
        <PhoneBtn />
        <BtnToTop />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/model2" element={<Model2 />} />
          <Route path="/vacancy" element={<Vacancy />} />

          <Route path="/cogeneration" element={<Cogeneration />} />
          <Route path="/powerengine" element={<Powerengine />} />
          <Route path="/co2" element={<Co2 />} />
          <Route path="/biogas" element={<Biogas />} />
          <Route path="/gas" element={<Gas />} />
          <Route path="/trigeneration" element={<Trigeneration />} />
          <Route path="/model3" element={<Model3 />} />
          <Route path="/model4" element={<Model4 />} />
          <Route path="/model6" element={<Model6 />} />
          <Route path="/diane" element={<Diane />} />
          <Route path="/news" element={<NewsPage />} />
          <Route path="/news1" element={<News1 />} />
          <Route path="/news2" element={<News2 />} />
          <Route path="/news3" element={<News3 />} />

          <Route path="/project" element={<Project />} />
        </Routes>

        <Footer />
      </Router>
    </>
  );
}
export default App;
