import React from "react";
import { TehnicalBlock, TehnicalContainer, Image } from "./FourEllements";

import { Save } from "@mui/icons-material";

import { LibraryModel } from "../../Library/LibraryModel";

import Cookies from "universal-cookie";
import { Button } from "@mui/material";

const cookie = new Cookies();

const FourBlock = ({ ellement }) => {
  let link =
    "/pdf/technical" + ellement + "-" + cookie.get("language") + ".pdf";
  let linkI =
    "/img/technical/technical" +
    ellement +
    "-" +
    cookie.get("language") +
    ".png";

  return (
    <>
      <TehnicalBlock>
        <TehnicalContainer>
          <Button
            href={link}
            target="_blank"
            variant="contained"
            sx={{ margin: 2 }}
            startIcon={<Save />}
          >
            {LibraryModel[cookie.get("language")].FourBlock}
          </Button>
          <Image src={linkI} />
        </TehnicalContainer>
      </TehnicalBlock>
    </>
  );
};

export default FourBlock;
