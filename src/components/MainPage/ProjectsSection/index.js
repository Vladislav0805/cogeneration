import React, { useEffect, useState } from "react";
import {
  ProductPage,
  ProductContainer,
  ProductHeader,
  SwipeContent,
  SlideText,
  SlideImage,
} from "./ProjectEllements";

import { Models } from "./Data";
import "./ProjectStyle.css";

import Cookies from "universal-cookie";
import { MainPageText } from "../../Library/LibraryMainPage";
import axios from "axios";
import { Swiper, SwiperSlide } from "swiper/react";
const cookies = new Cookies();

const ProductSection = () => {
  const [project, setProject] = useState([]);
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
  };
  useEffect(() => {
    (async () => {
      try {
        const { data } = await axios.post(
          "http://localhost:3001/api/v1/projects/list",
          {
            lang: cookies.get("language"),
          }
        );
        setProject(data);
      } catch (e) {
        console.log("error : ", e);
      }
    })();
  }, [cookies.get("language")]);

  return (
    <ProductPage id="project">
      <ProductContainer>
        <ProductHeader>
          {MainPageText[cookies.get("language")].project}
        </ProductHeader>
        <Swiper
          spaceBetween={50}
          navigation
          slidesPerView={5}
          className="mySwiper"
        >
          {project.map((item, id) => {
            return (
              <SwiperSlide key={id}>
                <SwipeContent to={`/project?id=${item.uuid}`}>
                  {item.pictures[0] && (
                    <SlideImage
                      src={`http://localhost:3001${item.pictures[0].path}`}
                      alt="image"
                    />
                  )}
                  <SlideText>{item.label}</SlideText>
                </SwipeContent>
              </SwiperSlide>
            );
          })}
        </Swiper>
        {/* <Swiper
          effect={"coverflow"}
          grabCursor={true}
          centeredSlides={true}
          slidesPerView={"auto"}
          loop={true}
          navigation
          autoplay={{
            delay: 8000,
            disableOnInteraction: true,
          }}
          coverflowEffect={{
            rotate: 30,
            stretch: 0,
            depth: 0,
            modifier: 1,
            slideShadows: false,
          }}
          pagination={true}
          className="mySwiper"
        >
          {project.map((item, id) => (
            <SwiperSlide key={id}>
              1234
              <SwipeContent to={item.link}>
                
              </SwipeContent>
            </SwiperSlide>
          ))}
        </Swiper> */}
      </ProductContainer>
    </ProductPage>
  );
};

export default ProductSection;
