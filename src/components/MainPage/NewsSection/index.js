import React, { useEffect, useState } from "react";
import {
  NewsPage,
  NewsContainer,
  NewsHeader,
  NewsContent,
  NewsFirstItem,
  FistImage,
  FirstText,
  NewsSecond,
  Second,
  SecondImg,
  SecondText,
} from "./NewsEllements";

import news1 from "../../../img/news/news1.jpg";
import news2 from "../../../img/news/news2.png";
import news3 from "../../../img/news/news3.jpg";
import Cookies from "universal-cookie";
import { MainPageText } from "../../Library/LibraryMainPage";
import axios from "axios";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import ListItemAvatar from "@mui/material/ListItemAvatar";
import Avatar from "@mui/material/Avatar";
import ImageIcon from "@mui/icons-material/Image";
import WorkIcon from "@mui/icons-material/Work";
import BeachAccessIcon from "@mui/icons-material/BeachAccess";
import NewspaperIcon from "@mui/icons-material/Newspaper";
import { Link } from "react-router-dom";

const cookies = new Cookies();
const NewsSection = () => {
  const [dataList, setData] = useState([]);
  useEffect(() => {
    (async () => {
      try {
        const { data } = await axios.post(
          "http://localhost:3001/api/v1/news/list",
          {
            lang: cookies.get("language"),
          }
        );
        setData(data);
      } catch (e) {
        console.log("error : ", e);
      }
    })();
  }, []);

  return (
    <NewsPage id="news">
      <NewsContainer>
        <NewsHeader>{MainPageText[cookies.get("language")].news}</NewsHeader>
        <List sx={{ width: "100%", maxWidth: 480 }}>
          {dataList.map((news) => (
            <Link to={`/news?id=${news.uuid}`}>
              <ListItem>
                <ListItemAvatar>
                  <Avatar
                    sx={{
                      width: 64,
                      height: 64,
                      backgroundColor: "transparent",
                    }}
                  >
                    <NewspaperIcon
                      sx={{ fontSize: "48px", color: "#008aff" }}
                    />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText
                  primary={news.label}
                  secondary={news.description}
                />
              </ListItem>
            </Link>
          ))}
        </List>
      </NewsContainer>
    </NewsPage>
  );
};

export default NewsSection;
