import React from 'react'
import { FooterContent } from './FooterEllements'

const Footer = () => {
    return (
        <FooterContent>
            Vipropat 2005-2023. ALL RIGHT RESERVED .
        </FooterContent>
    )
}

export default Footer
