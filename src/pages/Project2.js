import React from 'react'


import { ArtBlock, ArtContainer, FirstBlockArticles, FirstScreenLayer,  Li,  Text, TextArticlesBlock, TitleText } from '../components/ArticlesPage/ArticlesEllements';

import Cookies from 'universal-cookie';
import {ProjectText} from '../components/Library/ProjectLibrary'



import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/components/pagination/pagination.min.css"
import "swiper/components/navigation/navigation.min.css"

import Project2_1 from '../img/projects/p2/1.png'
import Project2_2 from '../img/projects/p2/2.png'
import Project2_3 from '../img/projects/p2/3.png'
import Project2_4 from '../img/projects/p2/4.png'


import SwiperCore, {
    Pagination,Navigation
  } from 'swiper/core';
import { SlideImage, SwipeContent } from '../components/Projects/ProjectsEllements';
  SwiperCore.use([Pagination,Navigation]);

const cookies = new Cookies();





const Project2 = () => {
    
    window.scrollTo(0, 0);

    return (
        <>
             <FirstBlockArticles>
                <FirstScreenLayer>
                <ArtContainer>
                    <ArtBlock>
                    {ProjectText[2][cookies.get('language')].title}
                    </ArtBlock>
                </ArtContainer>
                </FirstScreenLayer>
            </FirstBlockArticles> 
            <TextArticlesBlock>
                <ArtContainer>
                      <TitleText>{ProjectText[2][cookies.get('language')].text1}</TitleText>
                        <Text>{ProjectText[2][cookies.get('language')].text2}</Text>
                        <Li>{ProjectText[2][cookies.get('language')].text3}</Li>
                        <Li>{ProjectText[2][cookies.get('language')].text4}</Li>
                        <Li>{ProjectText[2][cookies.get('language')].text5}</Li>
                        <Li>{ProjectText[2][cookies.get('language')].text6}</Li>
                        <Li>{ProjectText[2][cookies.get('language')].text7}</Li>
                        <Li>{ProjectText[2][cookies.get('language')].text8}</Li>
                        <Li>{ProjectText[2][cookies.get('language')].text9}</Li>

                        <Text>{ProjectText[2][cookies.get('language')].text10}</Text>

                        <Text>{ProjectText[2][cookies.get('language')].text11}</Text>

                        <Text>{ProjectText[2][cookies.get('language')].text12}</Text>

                        <Swiper effect={'coverflow'} 
                        grabCursor={true} 
                        centeredSlides={true} 
                        slidesPerView={'auto'} 
                        loop={true} 
                        navigation
                        autoplay={{
                            "delay": 4000,
                            "disableOnInteraction": true
                          }} 
                        coverflowEffect={{"rotate": 30, "stretch": 0, "depth": 0,  "modifier": 1,  "slideShadows": false}} 
                        pagination={true} 
                        className="mySwiper">
                    
                    <SwiperSlide >
                        <SwipeContent href={Project2_1}  target="_blank">
                            <SlideImage src={Project2_1} />
                        </SwipeContent>
                    </SwiperSlide>
                    <SwiperSlide >
                        <SwipeContent  href={Project2_2}  target="_blank">
                            <SlideImage src={Project2_2} />
                        </SwipeContent>
                    </SwiperSlide>
                    <SwiperSlide>
                        <SwipeContent  href={Project2_3}  target="_blank">
                            <SlideImage src={Project2_3} />
                        </SwipeContent>
                    </SwiperSlide>
                    <SwiperSlide>
                        <SwipeContent  href={Project2_4}  target="_blank">
                            <SlideImage src={Project2_4} />
                        </SwipeContent>
                    </SwiperSlide>
                    
                </Swiper>

                        
                        <Text>{ProjectText[2][cookies.get('language')].text13}</Text>
                </ArtContainer>
               
           </TextArticlesBlock>



           

        </>
    )
}





export default Project2

