import React, { useEffect, useState } from "react";
import {
  FirstNews,
  News,
  NewsContainer,
  NewsContainerText,
  NewsHeadTitle,
  NewsScreenLayer,
} from "../components/NewsPages/NewsEllements";
import axios from "axios";
import { Box } from "@mui/material";

export default function NewsPage() {
  window.scrollTo(0, 0);
  const searchParams = new URLSearchParams(window.location.search);
  const uuid = searchParams.get("id");
  const [html, setHtml] = useState("");
  const [loading, setLoading] = useState(true);
  const [info, setInfo] = useState(null);

  useEffect(() => {
    if (!uuid) return;
    (async () => {
      try {
        const { data } = await axios.post(
          "http://localhost:3001/api/v1/news/getRecord",
          {
            uuid,
          }
        );
        setInfo(data);
        setLoading(false);
      } catch (e) {
        console.log("error : ", e);
      }
    })();
    console.log(uuid);
  }, []);

  return (
    <>
      <News>
        <FirstNews>
          <NewsScreenLayer>
            <NewsContainer>
              {loading ? (
                <NewsHeadTitle>Loading .... </NewsHeadTitle>
              ) : (
                <NewsHeadTitle>{info.label}</NewsHeadTitle>
              )}
            </NewsContainer>
          </NewsScreenLayer>
        </FirstNews>
        {info && info.html && (
          <NewsContainerText>
            <Box sx={{ p: 2 }}>
              <div dangerouslySetInnerHTML={{ __html: info.html }} />
            </Box>
          </NewsContainerText>
        )}
        {/* <NewsContainerText>
          <NewsText></NewsText>
          <NewsText>
            {NewsLibraryText.news1[cookies.get("language")].text1}
          </NewsText>

          <Swiper
            effect={"coverflow"}
            grabCursor={true}
            centeredSlides={true}
            slidesPerView={"auto"}
            loop={true}
            navigation
            autoplay={{
              delay: 4000,
              disableOnInteraction: true,
            }}
            coverflowEffect={{
              rotate: 30,
              stretch: 0,
              depth: 0,
              modifier: 1,
              slideShadows: false,
            }}
            pagination={true}
            className="mySwiper"
          >
            <SwiperSlide>
              <SwipeContent href={News1_1} target="_blank">
                <SlideImage src={News1_1} />
              </SwipeContent>
            </SwiperSlide>
            <SwiperSlide>
              <SwipeContent href={News1_2} target="_blank">
                <SlideImage src={News1_2} />
              </SwipeContent>
            </SwiperSlide>
            <SwiperSlide>
              <SwipeContent href={News1_3} target="_blank">
                <SlideImage src={News1_3} />
              </SwipeContent>
            </SwiperSlide>
            <SwiperSlide>
              <SwipeContent href={News1_4} target="_blank">
                <SlideImage src={News1_4} />
              </SwipeContent>
            </SwiperSlide>
            <SwiperSlide>
              <SwipeContent href={News1_5} target="_blank">
                <SlideImage src={News1_5} />
              </SwipeContent>
            </SwiperSlide>
          </Swiper>

          <NewsText>
            <strong>
              {NewsLibraryText.news1[cookies.get("language")].text2}
            </strong>
          </NewsText>

          <NewsAdditionalText>
            <AdditionalText>
              {NewsLibraryText.news1[cookies.get("language")].text3}
            </AdditionalText>
            <Li>{NewsLibraryText.news1[cookies.get("language")].text4}</Li>
            <LiText>
              {NewsLibraryText.news1[cookies.get("language")].text5}
            </LiText>
            <Li>{NewsLibraryText.news1[cookies.get("language")].text6}</Li>
            <LiText>
              {NewsLibraryText.news1[cookies.get("language")].text7}
            </LiText>
            <Li>{NewsLibraryText.news1[cookies.get("language")].text8}</Li>
            <LiText>
              {NewsLibraryText.news1[cookies.get("language")].text9}
            </LiText>
          </NewsAdditionalText>

          <NewsText>
            {NewsLibraryText.news1[cookies.get("language")].text10}
          </NewsText>
          <NewsFullImage src={News1_5} />
        </NewsContainerText> */}
      </News>
    </>
  );
}
