import React, { useEffect, useState } from "react";

import {
  ArtBlock,
  ArtContainer,
  FirstBlockArticles,
  FirstScreenLayer,
  Li,
  Text,
  TextArticlesBlock,
  TitleText,
} from "../components/ArticlesPage/ArticlesEllements";

import Cookies from "universal-cookie";
import { ProjectText } from "../components/Library/ProjectLibrary";

import { Swiper, SwiperSlide } from "swiper/react";

import "swiper/components/pagination/pagination.min.css";
import "swiper/components/navigation/navigation.min.css";

import SwiperCore, { Pagination, Navigation } from "swiper/core";
import {
  SlideImage,
  SwipeContent,
} from "../components/Projects/ProjectsEllements";
import axios from "axios";
import { useLocation } from "react-router-dom";
import { Image } from "antd";
SwiperCore.use([Pagination, Navigation]);

const cookies = new Cookies();

const Project = () => {
  window.scrollTo(0, 0);
  const [project, setProject] = useState(null);
  const location = useLocation();
  useEffect(() => {
    (async () => {
      try {
        const searchParams = new URLSearchParams(location.search);
        const id = searchParams.get("id");
        const { data } = await axios.get(
          `http://localhost:3001/api/v1/projects/${id}`
        );
        setProject(data);
      } catch (e) {
        console.log("error : ", e);
      }
    })();
  }, []);

  return project !== null ? (
    <>
      <FirstBlockArticles>
        <FirstScreenLayer>
          <ArtContainer>
            <ArtBlock>{project.label}</ArtBlock>
          </ArtContainer>
        </FirstScreenLayer>
      </FirstBlockArticles>
      <TextArticlesBlock>
        <ArtContainer>
          <div dangerouslySetInnerHTML={{ __html: project.html }}></div>

          <Swiper
            effect={"coverflow"}
            grabCursor={true}
            centeredSlides={true}
            slidesPerView={4}
            loop={true}
            navigation
            autoplay={{
              delay: 5000,
              disableOnInteraction: true,
            }}
            coverflowEffect={{
              rotate: 30,
              stretch: 0,
              depth: 0,
              modifier: 1,
              slideShadows: false,
            }}
            pagination={true}
            className="mySwiper"
          >
            {project.pictures.map((pic) => (
              <SwiperSlide>
                <Image
                  src={`http://localhost:3001${pic.path}`}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
                {/* <SlideImage src={`http://localhost:3001${pic.path}`} /> */}
              </SwiperSlide>
            ))}
          </Swiper>
        </ArtContainer>
      </TextArticlesBlock>
    </>
  ) : (
    <></>
  );
};

export default Project;
